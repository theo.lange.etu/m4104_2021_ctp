package fr.ulille.iutinfo.teletp;

import android.app.Application;
import android.content.Context;
import android.content.res.Resources;
import android.widget.Toast;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.lang.reflect.Array;

public class SuiviViewModel extends AndroidViewModel {

    private MutableLiveData<String> liveLocalisation;
    private MutableLiveData<String> liveUsername;
    private MutableLiveData<Integer> liveNextQuestion;
    private String[] questions;

    public SuiviViewModel(Application application) {
        super(application);
        liveLocalisation = new MutableLiveData<>();
        liveUsername = new MutableLiveData<>();
        liveNextQuestion = new MutableLiveData<>(0);
    }

    public LiveData<String> getLiveUsername() {
        return liveUsername;
    }

    public void setUsername(String username) {
        liveUsername.setValue(username);
        Context context = getApplication().getApplicationContext();
        Toast toast = Toast.makeText(context, "Username : " + username, Toast.LENGTH_LONG);
        toast.show();
    }

    public String getUsername() {
        return liveUsername.getValue();
    }
    public LiveData<String> getLiveLocalisation() {
        return liveLocalisation;
    }

    public void setLocalisation(String localisation) {
        liveLocalisation.setValue(localisation);
        Context context = getApplication().getApplicationContext();
        Toast toast = Toast.makeText(context, "Localisation : " + localisation, Toast.LENGTH_LONG);
        toast.show();
    }

    public String getLocalisation() {
        return liveLocalisation.getValue();
    }
    
    public void initQuestions(Context contexte) {
        this.questions = contexte.getResources().getStringArray(R.array.list_salles);
    }

    public String getQuestions(int position) {
        return this.questions[position];
    }

    LiveData<Integer> getLiveNextQuestion() {
        return this.liveNextQuestion;
    }

    void setNextQuestion(Integer nextQuestion) {
        this.liveNextQuestion.setValue(nextQuestion);
    }

    Integer getNextQuestion() {
        return this.liveNextQuestion.getValue();
    }
}
