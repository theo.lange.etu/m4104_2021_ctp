package fr.ulille.iutinfo.teletp;

import android.content.res.Resources;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.fragment.NavHostFragment;
import androidx.recyclerview.widget.RecyclerView;

public class VueGenerale extends Fragment implements AdapterView.OnItemSelectedListener {

    private String salle;
    private String poste;
    private String DISTANCIEL;

    private SuiviViewModel model;

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.vue_generale, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Resources rsc = getResources();

        DISTANCIEL = rsc.getStringArray(R.array.list_salles)[0];
        poste = "";
        salle = DISTANCIEL;
        model = new ViewModelProvider(requireActivity()).get(SuiviViewModel.class);




        Spinner spPoste = getActivity().findViewById(R.id.spPoste);
        spPoste.setOnItemSelectedListener(this);

        Spinner spSalle = getActivity().findViewById(R.id.spSalle);
        spSalle.setOnItemSelectedListener(this);

        ArrayAdapter<CharSequence> adapterPoste = ArrayAdapter.createFromResource(this.getContext(), R.array.list_postes, android.R.layout.simple_spinner_item);
        adapterPoste.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spPoste.setAdapter(adapterPoste);

        ArrayAdapter<CharSequence> adapterSalle = ArrayAdapter.createFromResource(this.getContext(), R.array.list_salles, android.R.layout.simple_spinner_item);
        adapterSalle.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spSalle.setAdapter(adapterSalle);

        view.findViewById(R.id.btnToListe).setOnClickListener(view1 -> {
            EditText etLogin = getActivity().findViewById(R.id.tvLogin);
            Toast login = Toast.makeText(getActivity(), etLogin.getText().toString(),Toast.LENGTH_LONG);
            login.show();
            NavHostFragment.findNavController(VueGenerale.this).navigate(R.id.generale_to_liste);
        });

        this.update();
        // TODO Q9
    }

    private void update() {
        Spinner spPoste = getActivity().findViewById(R.id.spPoste);
        Spinner spSalle = getActivity().findViewById(R.id.spSalle);
        if (this.salle == DISTANCIEL){
            spPoste.setVisibility(View.INVISIBLE);
            spPoste.setEnabled(false);
            model.setLocalisation(DISTANCIEL);
        } else {
            spPoste.setVisibility(View.VISIBLE);
            spPoste.setEnabled(true);
            model.setLocalisation(spPoste.getPrompt().toString()+ spSalle.getPrompt().toString());
        }


    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        if (id == R.id.spSalle) {
            salle = (String) parent.getItemAtPosition(position);
        } else {
            poste = (String) parent.getItemAtPosition(position);
        }
        this.update();
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
    // TODO Q9
}